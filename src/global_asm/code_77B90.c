#include <ultra64.h>
#include "functions.h"


#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80672E90.s")

/*
// TODO: Hmm, 64 bit nonsense?
extern s32 D_global_asm_807FB5A8;
extern s32 D_global_asm_807FB5AC;
extern s32 D_global_asm_807FB5B0;

void func_global_asm_80672E90(s32 arg0, s32 arg1, s32 arg2) {
    D_global_asm_807FB5A8 = arg0;
    D_global_asm_807FB5AC = arg1;
    D_global_asm_807FB5B0 = arg2;
}
*/

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80672EBC.s")

/*
// TODO: Hmm, 64 bit nonsense?
void func_global_asm_80672F94(s16, s64);
extern s32 D_global_asm_807FB5E0;
extern s32 D_global_asm_807FB5E4;
extern s16 D_global_asm_807FB5E8;
extern s16 D_global_asm_807FB5F4;

void func_global_asm_80672EBC(s64 arg0, s64 arg2) {
    s64 sp10;
    s64 sp8;
    s16 var_a0;
    s64 var_a2;

    var_a2 = arg2;
    sp8 = arg0;
    sp10 = var_a2;
    if ((arg0 > 0) && (var_a2 > 0)) {
        var_a2 = (var_a2 / D_global_asm_807FB5E4) * D_global_asm_807FB5E8;
        var_a0 = (arg0 / D_global_asm_807FB5E0) + var_a2;
    } else {
        var_a0 = 0;
    }
    D_global_asm_807FB5F4 = var_a0;
    func_global_asm_80672F94(var_a0, var_a2);
}
*/

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80672F94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806730A4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673240.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806732A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673324.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806734E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673708.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806738D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806739D8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673A40.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673A94.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673B78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673C34.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673D48.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80673FDC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674080.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674150.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806742C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674330.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_8067443C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_806744A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_8067457C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674688.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_8067470C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674884.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674A5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_77B90/func_global_asm_80674ADC.s")
