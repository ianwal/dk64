#include <ultra64.h>
#include "functions.h"

extern s8 D_global_asm_80750660;
extern s8 D_global_asm_807506A4;

extern s8 D_global_asm_807FC8D0;

// TODO: Add to aaD union
typedef struct {
    s16 unk0;
    s8 unk2; // keyIndex
    s8 unk3;
    Actor *unk4;
} PadlockAAD;

void func_global_asm_806BD170(void) {
    Actor* highest_priority_padlock;
    s16 keyIndex;
    s16 sp50;
    s16 keysCollected;

    sp50 = -1;
    keyIndex = 0;
    keysCollected = 0;
    while (keyIndex < 8) {
        // Key[keyIndex] Turned Flag
        if (!isFlagSet(0x1BC + keyIndex, FLAG_TYPE_PERMANENT)) {
            PadlockAAD *temp_v0;
            s16 temp2 = 260 + (keyIndex * 512);
            // Spawn Padlock
            spawnActor(ACTOR_PADLOCK_KLUMSY, 0xCD);
            last_spawned_actor->draw_distance = 2000;
            last_spawned_actor->object_properties_bitfield |= 0x800400;
            last_spawned_actor->object_properties_bitfield &= ~0x8000;
            
            last_spawned_actor->unk16A = 0xFF;
            last_spawned_actor->unk16B = 0xFF;
            last_spawned_actor->unk16C = 0xFF;
            last_spawned_actor->shadow_opacity = 0xFF;

            last_spawned_actor->x_position = (func_global_asm_80612794(temp2) * 270.0f) + current_actor_pointer->x_position;
            last_spawned_actor->z_position = (func_global_asm_80612790(temp2) * 270.0f) + current_actor_pointer->z_position;
            last_spawned_actor->y_position = 100.0f;
            last_spawned_actor->y_rotation = temp2;
            last_spawned_actor->unk146 = 0;
            last_spawned_actor->control_state = 0;
            temp_v0 = last_spawned_actor->additional_actor_data;
            temp_v0->unk0 = 0;
            temp_v0->unk4 = current_actor_pointer;
            temp_v0->unk2 = keyIndex;
            // Key Collected Flag
            if (isFlagSet(D_global_asm_80744710[keyIndex], FLAG_TYPE_PERMANENT)) {
                sp50 = keyIndex;
                highest_priority_padlock = last_spawned_actor;
                highest_priority_padlock->control_state = 5;
                keysCollected++;
            }
        }
        keyIndex++;
    }
    D_global_asm_807506A4 = 0;
    // Isles: Japes Boulder Smashed
    if (!isFlagSet(0x1BB, FLAG_TYPE_PERMANENT)) {
        sp50 = -1;
    }
    if (!D_global_asm_80750660) {
        s32 temp = 1;
        if (sp50 != -1) {
            highest_priority_padlock->unk146 = temp;
            highest_priority_padlock->control_state = 1;
            D_global_asm_80750660 = 1;
            D_global_asm_807FC8D0 = sp50;
        }
    }
}

// close
#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_C1E70/func_global_asm_806BD3E4.s")

extern s8 D_global_asm_807506B4;
extern s8 D_global_asm_807506A8[];

extern s16 D_8076A0A6; // Probably static

extern f32 D_807FC8D4;

s32 areAllKeysTurnedIn(void);

typedef struct {
    Actor* unk0;
} Actor178_806BD3E4;

/*
void func_global_asm_806BD3E4(void) {
    Actor178_806BD3E4 *a178;
    s32 sp30;
    s32 sp2C;
    u8 sp2B;
    u8 temp;
    u16 sp28;
    s16 var_v1; // Probably static/volatile
    s16 var_t0;

    var_v1 = D_8076A0A6;
    a178 = current_actor_pointer->unk178;
    if (!(current_actor_pointer->object_properties_bitfield & 0x10)) {
        if (areAllKeysTurnedIn()) {
            current_actor_pointer->control_state = 0x40;
            return;
        }
        current_actor_pointer->draw_distance = 2000;
        current_actor_pointer->object_properties_bitfield |= 0x400;
        var_v1 = 0;
        D_global_asm_807506B4 = 0;
        spawnActor(ACTOR_KLUMSY_CAGE, 0xC4);
        moveAndScaleActorToAnother(last_spawned_actor, current_actor_pointer, current_actor_pointer->animation_state->scale_y);
        last_spawned_actor->unk64 |= 0x20;
        last_spawned_actor->object_properties_bitfield |= 0x400;
        last_spawned_actor->draw_distance = 0x7D0;
        a178->unk0 = last_spawned_actor;
        D_807FC8D4 = 1000.0f;
        func_global_asm_806BD170();
        current_actor_pointer->control_state = 0;
        if (D_global_asm_80750660 != 0) {
            D_global_asm_80750660 = 0;
        }
        if (isFlagSet(0x1BB, FLAG_TYPE_PERMANENT) == 0) {
            current_actor_pointer->control_state = 1;
        }
    }
    var_t0 = -1;
    if (var_v1) {
        var_t0 = 0;
        while (!(var_v1 & 1)) {
            var_v1 >>= 1;
            var_t0++;
        }
    }
    D_807FC8D4 = ((current_actor_pointer->x_position - player_pointer->x_position) * (current_actor_pointer->x_position - player_pointer->x_position)) + ((current_actor_pointer->z_position - player_pointer->z_position) * (current_actor_pointer->z_position - player_pointer->z_position));
    sp28 = 0;
    switch (current_actor_pointer->control_state) {
        case 1:
            playActorAnimation(current_actor_pointer, 0x2EF);
            current_actor_pointer->control_state = 2;
            playCutscene(player_pointer, 2, 1);
            break;
        case 2:
            if (func_global_asm_80629148()) {
                playSong(0x7D, 1.0f);
                playActorAnimation(current_actor_pointer, 0x2F1);
                current_actor_pointer->control_state = 3;
                current_actor_pointer->control_state_progress = 0;
            }
            break;
        case 3:
            if (current_actor_pointer->control_state_progress & 1) {
                current_actor_pointer->control_state_progress++;
                sp2B = 0xF;sp28 = 0x28;
            }
            break;
    }
    if ((func_global_asm_8061CB50() == 0) && (current_actor_pointer->animation_state->unk64 != 0x2F0)) {
        playActorAnimation(current_actor_pointer, 0x2F0);
    }
    if (sp28) {
        func_global_asm_8061F0B0(D_global_asm_807F5D10, sp2B, sp28);
    }
    if (var_t0 >= 0) {
        if (var_t0 == D_global_asm_807506A8[D_global_asm_807506B4]) {
            if (D_global_asm_807506A8[++D_global_asm_807506B4] == -1) {
                playSound(0x1DA, 0x7FFF, 63.0f, 1.0f, 5, 0);
                setFlag(0x28, TRUE, FLAG_TYPE_GLOBAL);
                setFlag(0x26, TRUE, FLAG_TYPE_GLOBAL);
                D_global_asm_807506B4 = -1;
            }
        } else {
            D_global_asm_807506B4 = 0;
        }
    }
    D_8076A0A6 = 0;
    renderActor(current_actor_pointer, 0);
}
*/

// Jumptable, doable, close, regalloc
#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_C1E70/func_global_asm_806BD7B0.s")

extern s32 D_global_asm_80720A7C; // TODO: Sprite

typedef struct {
    s16 unk0;
    s16 unk2;
} Struct80750664;

extern Struct80750664 D_global_asm_80750664[];
extern s16 D_global_asm_80750684[];
extern s8 D_global_asm_807506A4;

typedef struct {
    union {
        struct {
            s32 unk0;
            s32 unk4;
        };
        s8 unk0_s8arr[8];
    };
} Struct807506B8;

extern Struct807506B8 D_global_asm_807506B8;

typedef struct {
    s16 unk0;
    s8 unk2;
    s8 unk3;
    Actor *unk4;
} AAD_806BD7B0;

typedef struct {
    Actor *unk0;
} A178_806BD7B0;

/*
void func_global_asm_806BD7B0(void) {
    AAD_806BD7B0 *aaD;
    f32 sp48;
    f32 sp44;
    Actor *temp_v0_2;
    Struct80750664 *temp_v0_3;
    s32 sp38;
    Struct807506B8 sp30;

    aaD = current_actor_pointer->additional_actor_data;
    switch (current_actor_pointer->control_state) {
        case 5:
            if ((D_global_asm_807506A4 != 0) && (func_global_asm_8061CB50() == 0)) {
                aaD->unk0++;
                if (aaD->unk0 >= 0x29) {
                    D_global_asm_807506A4 = 0;
                    current_actor_pointer->control_state = 1;
                }
            } else {
                aaD->unk0 = 0;
            }
            break;
        case 1:
            aaD->unk0 = 0;
            current_actor_pointer->control_state = 2;
            break;
        case 2:
            if (D_807FC8D4 < 160000.0) {
                current_actor_pointer->control_state = 3;
                aaD->unk0 = 0;
                playSong(0x3B, 1.0f);
                playSound(0x258, 0x7FFF, 63.0f, 1.0f, 5, 0);
                playCutscene(current_actor_pointer, 0, 1);
                playActorAnimation(current_actor_pointer, 0x291);
                current_actor_pointer->control_state_progress = 0;
                current_actor_pointer->unk146 = 1;
            }
            break;
        case 3:
            if (current_actor_pointer->control_state_progress == 1) {
                current_actor_pointer->control_state_progress++;
                break;
            }
            if (current_actor_pointer->control_state_progress == 2) {
                func_global_asm_80602B60(0x3B, 1);
                current_actor_pointer->shadow_opacity -= 8;
                if (current_actor_pointer->shadow_opacity <= 0) {
                    current_actor_pointer->shadow_opacity = 0;
                    aaD->unk0++;
                    if (aaD->unk0 == 1) {
                        playSound(0x1F6, 0x7FFF, 63.0f, 1.0f, 5, 0);
                    }
                    if (aaD->unk0 >= 0xB) {
                        sp30 = D_global_asm_807506B8;
                        current_actor_pointer->control_state = 4;
                        playSong(0x7D, 1.0f);
                        playActorAnimation(aaD->unk4, D_global_asm_80750684[aaD->unk2]);
                        aaD->unk0 = 0;
                        aaD->unk4->control_state = 3;
                        aaD->unk4->control_state_progress = 0;
                        playCutscene(NULL, 1, 1);
                        func_global_asm_8063DA40(sp30.unk0_s8arr[aaD->unk2], 2);
                    }
                }
                if (current_actor_pointer->shadow_opacity < 0x80) {
                    current_actor_pointer->y_position -= 6.0;
                }
            } else {
                func_global_asm_80714950(0x10064);
                func_global_asm_8071498C(func_global_asm_8071AADC);
                changeActorColor(0xFF, 0xFF, 0xFF, 0xFF);
                sp48 = (func_global_asm_806119FC() * 30.0) + current_actor_pointer->x_position;
                sp44 = (func_global_asm_806119FC() * 30.0) + (current_actor_pointer->y_position + 20.0);
                drawSpriteAtPosition(&D_global_asm_80720A7C, 1.2f, sp48, sp44, (func_global_asm_806119FC() * 30.0) + current_actor_pointer->z_position);
                break;
            }
            break;
        case 4:
            if (areAllKeysTurnedIn() != 0) {
                temp_v0_2 = ((A178_806BD7B0*)aaD->unk4->unk178)->unk0;
                temp_v0_2->y_position += 5.0;
                if (aaD->unk0 == 0x10E) {
                    setFlag(0x315, TRUE, FLAG_TYPE_PERMANENT);
                    func_global_asm_80712524(D_global_asm_80750664[7].unk0, D_global_asm_80750664[7].unk2);
                }
            }
            sp38 = 0xB4;
            if ((aaD->unk2 == 2) || (aaD->unk2 == 7)) {
                sp38 = 0xB4;
                if (areAllKeysTurnedIn() == 0) {
                    sp38 = 0x5A;
                }
            }
            aaD->unk0++;
            if (sp38 == aaD->unk0) {
                if ((aaD->unk2 == 2) || (aaD->unk2 == 7)) {
                    setFlag(aaD->unk2 + 0x1BC, TRUE, 0);
                    if (areAllKeysTurnedIn() == 0) {
                        func_global_asm_8061CB08();
                        func_global_asm_80602B60(0x7D, 1);
                        D_global_asm_807506A4 = 1;
                    }
                } else {
                    temp_v0_3 = &D_global_asm_80750664[aaD->unk2];
                    func_global_asm_80712524(temp_v0_3->unk0, temp_v0_3->unk2);
                }
            }
            break;
    }
    current_actor_pointer->unk15E = 0x28;
    renderActor(current_actor_pointer, 0);
}
*/

s32 areAllKeysTurnedIn(void) {
    s32 key;
    for (key = 0; key < 8; key++) {
        if (!isFlagSet(key + 0x1BC, FLAG_TYPE_PERMANENT)) {
            return FALSE;
        }
    }
    return TRUE;
}
